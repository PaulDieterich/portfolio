$( document ).ready(function() {
    
    // menu icon - links in der ecke für mobile geräte
    for(var i = 0; i < 3; i++){
    $( ".toggle-btn" ).append(  '<span class="btn-span"></span>');
    }
    //menu liste
    var menuList = [
        "uebermich","projekte","skills"
    ];
    for(var i = 0; i< menuList.length; i++){
        $("ul").append('<li> <a class="item" href="#'+ menuList[i]+'"> ' +menuList[i]+' </a> </li>');
        $(".ui.inverted.menu").append("<a class='item' href='#"+ menuList[i] +"'>"+ menuList[i] +"</a>");
      
    }
        
    //Menu aufruf in der Mobilen ansicht
    $(".toggle-btn").on("click", function(){
        $(".sidebar").toggleClass("active");
       
    });
    // bei clicken auf .sidebar a tag geht die seidbar zurück           
    $(".sidebar a").on("click",function(){
        $(".sidebar").toggleClass("active");
     
    });

    //Navbar erscheint ab 750px und bleibt fest Oben
 $(this).scroll(function(){
    if( $("body").scrollTop() > 750 || $(document.documentElement).scrollTop() > 750 ){
       console.log(" YEA-" +  $(document.documentElement).scrollTop());
        $(".navbar").addClass("showNavbar");
    }else{
        console.log("NOPE -" +  $(document.documentElement).scrollTop());
        $(".navbar").removeClass("showNavbar");
    }
 
 
 });

 /* ************************************************************************ */
 /* ********************** PROJECT GENERATOR ************************ */
  /* ************************************************************************ */
    var project = [
        {

            name: "portfolio",
            tags: ["HTML ", "CSS ", "JS "],
            kursInfo: "Lorem ipsum dolor amet man bun banjo heirloom cold-pressed. Chambray farm-to-table echo park distillery aliquip, fam post-ironic esse ramps. Air plant sint ea activated charcoal microdosing squid unicorn cliche trust fund butcher. Sartorial iPhone jean shorts distillery four loko paleo. Bushwick twee voluptate, nisi fanny pack salvia prism activated charcoal occaecat kombucha craft beer. Tumeric deep v actually cardigan authentic franzen migas, pok pok fam godard copper mug",
            text: " ", 
            git: ""
        },
        {
            name: "portfolio 2",
            tags: ["EJS ", "CSS " , "JS " , "Node.js ", "Express "],
            kursInfo: "Lorem ipsum dolor amet man bun banjo heirloom cold-pressed. Chambray farm-to-table echo park distillery aliquip, fam post-ironic esse ramps. Air plant sint ea activated charcoal microdosing squid unicorn cliche trust fund butcher. Sartorial iPhone jean shorts distillery four loko paleo. Bushwick twee voluptate, nisi fanny pack salvia prism activated charcoal occaecat kombucha craft beer. Tumeric deep v actually cardigan authentic franzen migas, pok pok fam godard copper mug",
            text: " ", 
            git: ""
        }

    ]
    var durchlaufVariable = 0;
    project.forEach(function(data ){
        durchlaufVariable++;     
        $(".projekt").append("<div class='col col-mid-12'>" +
        "<div class='ownCard'>" +
            "<div class='card'>" +
                "<div class='content'>" +
                    "<div class='header'> " +data.name +"</div>" +
                    
                    "<div class='meta "+  durchlaufVariable +"'></div> " + 
            
                    "<div class='description'>" + data.kursInfo + "</div>" +
                "</div>" +
                "<div class='cardButton'> <button id=" + data.name+"> Mehr Info </button> </div>" +
            "</div>" +
        "</div>"
       
        );
       
        $(".meta").append("<span> " + data.tags + "</span>");
        $( '#' + data.name  ).click(function() {
          
            alert( "Handler for .click() called." + data.name +"" );
         
        });
        
    });
     
   
   
});;

   
     
/*
   //Scroll effekt - https://pixelbar.be/blog/mit-jquery-scrollen-gewusst-wie/
    $('a[href^=#]').on('click', function(e){
        var href = $(this).attr('href');
        $('html, body').animate({
            scrollTop:$(href).offset().top
        },'slow');
        e.preventDefault();
    });

*/


